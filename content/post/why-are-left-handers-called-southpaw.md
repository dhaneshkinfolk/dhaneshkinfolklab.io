---
title: "Why Are Left Handers Called Southpaw"
date: 2019-02-01T22:15:08+05:30
categories:
- diary
- article
tags:
#- tag2
keywords:
#- tech
#thumbnailImage: //example.com/image.jpg
---
Being a fan of left hand cricket batsmen like Sourav ganguly and Yuvraj Singh, I came across the term 'southpaw' many a times, but never really bothered about finding the reason for the term. What is the relation of south and left. If it was west, I could have related it with the the normal represenation of compass points. Let us find out the real reason.
<!--more-->
Interestingly, there are many arguments about the origin of this usage. Most of them trace back to either baseball or boxing. Cricket must have got it from baseball (this is my assumtion).
Let me copy/paste the reasons: 
* Baseball origin
  Baseball parks(ground) were traditionally constructed in a way, so that the batter faced east to minimize glare from the sun as it set. A result was that as the batter faced the mound, right-handed pitchers' pitching arms were to the north of the pitcher and left-handed pitchers' pitching arms were to the south of the pitcher. In simple words, compass points are rotated 90 degree as a result of the arrangement. 
* Boxing origin  
  In boxing, the term refers to the stance where the boxer has his right hand and right foot forward, leading with right jabs, and following with a left cross right hook. n such a stance the main hand, in this case the left, is below, (facing south) hence south "paw"
  [main references : https://www.quora.com/Why-are-lefties-called-South-Paw , https://www.quora.com/Why-are-left-handed-people-referred-to-as-southpaws, https://www.history.com/news/why-are-left-handers-called-southpaws ]
  
Nothing much happened today. Well, I took a shower and washed my cloths. That is not something that happens everyday. I was feeling really sleepy today, eventhough I got enough sleep yesterday night. That's all for today.

*6 / 21 days of continous posts on this website*

```
𝗧𝗼𝗱𝗮𝘆' 𝗖𝗵𝗲𝗰𝗸𝗹𝗶𝘀𝘁
🗹 Finger Whistling
🗹 Blog
🗹 Bending
☐ Flute
☐ Electronics
☐ Meditation
🗹 Vim
🗹 Chess
```

