---
title: "Vaginal Fluids"
date: 2019-01-29T23:11:14+05:30
categories:
- article
- networking
tags:
- l2s2l
keywords:
#- gk
#thumbnailImage: //example.com/image.jpg
---
Until recently, I was not even aware of any normal/healthy vaginal dicharges other than menstrual blood. When I came to know about it from a friend, I started to search on this subject and it turns out we can create a rainbow using different vaginal discharges. This is a summary of what I found on the Internet. There could be lots of false information in this post. My sources are not authoritative and I have zero experience in this subject(obviously). **Please don't depend on this post.**
<!--more-->
Usually vaginal discharges starts after puberty. There are a variety of types and colors of vaginal fluids. Here are some types I found.

### Brown blood
It is quiet common during period. Usually it is cervical mucus and old blood, which smells like iron. It most likely will become more red during the period itself. 

Brown discharge could also be caused by pregnancy, manopause(pre), PCODs, STDs and ofcourse Cervical Cancer(Everything is a symptom of cancer. If you don't trust me, just search the Internet with any physical condition you have, there will be atleast one article saying it as a symptom of cancer).

### Thick and white
This is common just before (or just after) period. If it is accompanied by itching, it could be yeast infection.

### Clear and Watery
This could be sweat or the fluids that keeps the mucus membrane in the vagina and vulva, which keeps dirts and germs from getting into body (like the fluids find in the nostrils).

### Clear and slippery
This is usually the lubrication fluid produced during sexual excitation. This is the one that reduces friction during sex. 

### Clear and Egg white consistency 
This is the ovulation discharge. It is some times called Egg White Cervical Mucus(EVCM) due to its appearance. This is caused by the hormonal changes in the body when egg is released from ovary to fallopian tube. Body is most fertile during this time. The discharge around the time of ovulation helps the sperm fertilize the released egg.

### Thick, white, odorless discharge, similar to cottage cheese
This is usually a sign of bacterial or yeast infection. In that case it will be accompanied by swelling and pain around the vulva, itching and painful sexual intercourse

### Yellow or Green discharge
This can be a sign of infection, especially if it’s chunky like cottage cheese, or has a foul smell. It is always better to consult a doctor if discharge is of this color.

### Thin and grayish white
This could be due to Bacterial Vaginosis(BV), in that case it will have a fishy smell.

Apart from the above mentioned ones, STIs can cause different types of discharges. For example gonorrhea can produce yellowish, bloody discharge and trichonomisis can produce clear, white, yellowish, or greenish with an unusual fishy smell.

*3 / 21 days of continous posts on this website*



