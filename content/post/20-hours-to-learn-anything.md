---
title: "20 Hours to Learn Anything"
date: 2019-01-30T08:48:06+05:30
categories:
- diary
#- subcategory
tags:
- 20toLearn
keywords:
#- tech
#thumbnailImage: //example.com/image.jpg
---
Few months ago I watched a TEDx talk by Josh Kaufman, explaining his approach on learning new skills. In the talk he explains, how he manages to learn a new skill within 20hours over the period of one month. Many people got inspired by his talk(and his book), and succeeded in developing new skills. Me tried it. Me being me abonded it the midways many times. This time I am trying to stick with the plan and develop new skills.
<!--more-->
I will try to acquire one skill and a time by following the #20toLearn method, but will try to incorporate some habits which are part of my work or everyday life. From today onwards I will append a list at the end of each post to keep track of my progress. Here is the list of things I am currently trying ot achieve.

* Finger Whistling
I learned to whistle more than 20 years ago, but never succeeded in whistling with fingers. I was too afraid of failure or too lazy to try. I am practicing this during the one minute breaks, I take while working in front of the computer(I use safeeyes to remind me to take break).
* Blog/Diary
Writing on this site
* Bending
I have (or should I say, had) a comparatively flexible body(that's what I think), but I never succeeded in touching the ground by bending forward. I practice this one too during my one minute breaks.
* Flute #20toLearn
I wanted to play flute for years. I even bought a flute 14 years to start practicing. But I never started it. This is my current 20 hours to learn challenge.
* Electronics
I was always fascinated by electronics, but never managed to do anything in it. I learn electronics during my free time.
* Meditation
Well, you know what it is. Currently I am using Headspace app for meditation.
* Vim
Sometimes I have to work in situations where the only available editor is Vim. I am trying to get the hang of it by writing posts in sites using vim. 
* Chess 
A friend brought a chess board from home on my request. Trying to figure out the game. 

*4 / 21 days of continous posts on this website*

```
𝗧𝗼𝗱𝗮𝘆' 𝗖𝗵𝗲𝗰𝗸𝗹𝗶𝘀𝘁
☐ Finger Whistling
🗹 Blog
☐ Bending
☐ Flute
☐ Electronics
🗹 Meditation
🗹 Vim
🗹 Chess
```

