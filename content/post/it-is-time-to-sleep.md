---
title: "It is time to Sleep"
date: 2019-02-08T23:58:14+05:30
categories:
- diary
tags:
- excuse
#thumbnailImage: //example.com/image.jpg
---
I haven't had a good sleep in days. I have been trying to sleep the whole day. Managed to doze at evening, but someone woke me up. Still in a half asleep mode.
<!--more-->
I can push myself to write a post and check some more items in the checklist, that will ruin another nights sleep. It will be better if I try to sleep - a talk on quanum physics and string theory should do the trick. good night...

*5 / 21 days of continous posts on this website*

```
𝗧𝗼𝗱𝗮𝘆' 𝗖𝗵𝗲𝗰𝗸𝗹𝗶𝘀𝘁
🗹 Finger Whistling
🗹 Blog
☐ Bending
☐ Flute
☐ Electronics
☐ Meditation
☐ Vim
☐ Chess
```
