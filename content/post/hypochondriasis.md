---
title: "Hypochondriasis"
date: 2019-02-05T22:22:33+05:30
categories:
- diary
#- subcategory
#thumbnailImage: //example.com/image.jpg
---
Another boring/frustating day is coming to an end. Pretty much nothing happened today. Still trying to not do the job, I hate. Well, there was something new. I started cooking after months of gap. While searching for something to write about, I found the word hypochondriasis scribbled in one of my todo lists. I do that a lot. When I want to look up something, but too lazy to do that a just write it down somewhere to look up later.
<!--more-->
So, basically it is a condition where someone interprets minor physical and mental symptons as that of serious illness along with constant self examination and diagonosis. I got interested in this, since I exhibit most of its symptoms (or maybe I am misdiagonising myself here, interestingly that too is a symptom of hypochondriasis :p). My fear is mostly powered by internet and people trying to educate others about some disease. I always finds some symptoms to self diagonise myself with the disease I am reading about. And suring the internet for sometime will confirm it. If you have any doubt just browse webmd - everything is a symptom of cancer. Like one of the symptom given in the wikipedia page, I wont consult a doctor to confirm my condition. 

*2 / 21 days of continous posts on this website*

```
𝗧𝗼𝗱𝗮𝘆' 𝗖𝗵𝗲𝗰𝗸𝗹𝗶𝘀𝘁
🗹 Finger Whistling
🗹 Blog
🗹 Bending
▣ Flute
🗹 Electronics
🗹 Meditation
🗹 Vim
🗹 Chess
```
