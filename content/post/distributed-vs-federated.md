---
title: "Distributed vs Federated"
date: 2019-02-04T22:15:59+05:30
categories:
- networking
tags:
- l2s2l
- todo
keywords:
- tech
#thumbnailImage: //example.com/image.jpg
---
I missed the last two day's since I didn't had access to a properly working computer. And I missed almost every items in my checklist, except whistling - which I practiced for hours and still can't do it right. These days I have been hearing a lot about non centralized networks, and the terms distributed and federated pops up here and there. 
<!--more-->
After digging for sometime, it became clear that there is no single agreed-upon definition for federated networks. What follows is the definition I am okay with.

In terms of network architecture, node communication and distribution can be categrorized into three **for our purpose**
## Centralied
All the nodes communicate via a central server. Here, failure of central server results in failure of the whole system

## Distributed networks
In this case, there won't be a central server. Each node is coneected to some other nodes and communication between nodes happen by hoping through different nodes. There will be some mechanisms to find the shortest path. Here there is no single point of failure and every system has equal role(ideally). Sometimes, this type of network is termed as **p2p (peer to peer)** network.

## Decentralized
In simple terms this is a distributed network of centralized networks. Each individual network will have a server and different servers communicate with each other on an agreed upon protocol to provide the clients with resources from all the resources. This shoud be the federated network. Even though there are dispute about it, this definition is okay with me..  

In general usage the term federated is mostly concerned with the data exchange between multiple networks on an agreed upon method

*OK.... Let us restart it from the beginning....
:1 / 21 days of continous posts on this website*

```
𝗧𝗼𝗱𝗮𝘆' 𝗖𝗵𝗲𝗰𝗸𝗹𝗶𝘀𝘁
🗹 Finger Whistling
🗹 Blog
🗹 Bending
☐ Flute
☐ Electronics
🗹 Meditation
🗹 Vim
🗹 Chess
```


