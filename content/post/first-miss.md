---
title: "I missed today"
date: 2019-01-26T22:10:28+05:30
categories:
- diary
#- subcategory
tags:
- excuse
#thumbnailImage: //example.com/image.jpg
---
My actual plan for today was to write an article about ASN then decided to write about republic day. I was feeling sleepy the whole day and I could not complete any of the articles.
<!--more-->
Today morning one friend of mine asked for a help with a computer program. Usually when he asks for help I will wander around in the internet and sends him some links to solve the problem. Since I am on a quest to redeem myself, I decided to help him by working on it. My laptop lost internet connectivity while I was tinkering with it and I spend some hours to resolve the issue. Then I was feeling sleepy and tried to sleep but could not sleep. So started writing today's article, but could not read on the topic as I was half asleep. 

I guess the program I was writing for my friend is now in working condition. It is time for me to go to sleep. Let us reset the progress meter to zero. 

*0/21 days of continous posts on this website*
