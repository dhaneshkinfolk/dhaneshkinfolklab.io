---
title: "IPTables"
date: 2019-02-06T23:22:10+05:30
categories:
- diary
- article
- networking
tags:
- l2s2l
- todo
keywords:
- tech
#thumbnailImage: //example.com/image.jpg
---
I have been planning to setup an ubuntu system, in a motherboard with two ethernet ports, as router for months. As part of my enlightenment, I decided to setup the system. Every tutorial I looked up online listed 'knowledge of iptables' as the only prerequisite. This is my attemp to get a basic idea about it so that I can set up the router tomorrow. 
<!--more-->
IPTables is the userspace interface to linux kernel firewall. The only firewall I have used before in linux is ufw (via terminal and GUIs), which is a command line front end for iptables - uncomplicating the usage and configuraion (ufw stands for Uncomplicated Firewall). 

The firewall rules are stored in 5 chains, of which we commonly use 3 - INPUT, OUTPUT and FORWARD. The names are self explanatory. The rules are also arranged in tables namely - Filter table (The most used table), NAT, Mangle Table, RAW. We will just focus on filter table for now. Current list can be viewed by using the command
```
sudo iptables -L
```
The most common operations of insertion and deletion can be performed using the following commands:

* ### Insertion
  ```
  sudo iptables -I INPUT [<position>] -t filter -s [<ip address/ network address>] -j DROP
  ```
  here **-I** stands for *insert*, INPUT is the chain, **-t** for *table* (filter in this case) followed with ip or network address. **-j** stands for *jump to action* it can be either of DROP, REJECT and ACCEPT. Additonal fine tuning can be performed by specifying protocol using **-p** flag and destination port using **-dport** flag. **-A** can be used in place of *-I* to *append* at the end of table. -I without position will insert at the first position. 
* ### Deletion
  ```
  sudo iptables -D INPUT <postion> -t filter
  ```
  Here **-D** stands for *Delete*. Everything else is same as the one discussed above. 
  
*3 / 21 days of continous posts on this website*

```
𝗧𝗼𝗱𝗮𝘆' 𝗖𝗵𝗲𝗰𝗸𝗹𝗶𝘀𝘁
🗹 Finger Whistling
🗹 Blog
☐ Bending
☐ Flute
☐ Electronics
☐ Meditation
🗹 Vim
🗹 Chess
```
