---
title: "Setting up Ubuntu 18.04 as a Simple Router"
date: 2019-02-08T23:29:21+05:30
categories:
- diary
- article
- networking
tags:
- l2s2l
- todo
- diy
keywords:
- tech
#thumbnailImage: //example.com/image.jpg
---
This post was supposed to be posted yesteday. It was written yesterday (well, technically - early morning, today). I was trying to set up my laptop and I am still not happy with the result. I am not considering this as a break in my attempt to write continuous posts (Well, it is my website - my website, my rule :p). While I was getting frustated with efi, trisquel and toshiba firmware settings, I started working on the ubuntu router project. There were tutotials titled *the easiest method*, but instructions were confusing. This is the one that worked for me...
<!--more-->
This post is not for networking experts or terminal gurus. This is for people like me, who doesn't mind working with terminals but spends most of the time in GUIs. This one is based on [this](https://www.ascinc.com/blog/linux/how-to-build-a-simple-router-with-ubuntu-server-18-04-1-lts-bionic-beaver/) tutorial. The tutorial is for ubuntu server. The tutotial didn't work for me, I had to make some small changes to make it work in desktop ubuntu. This is not my final ubuntu router project, I will do it soon (hopefully).
## Prerequisites
* A computer running Ubuntu 18.04 (I used Xubuntu)
* At least two network interfaces

## Network Interfaces Configuration
My system has two interfaces, namely *enp1s0* & *enp2s0* (use **ifconfig** to find yours). I will be using enp1s0 for WAN connection and enp2s0 for LAN connection. Replace every occurance of interface name with that of yours. Connections can be configured using network manager applet.

* Connect to internet via WAN interface

![WAN configuration-1](/img/wan_1.png)

![WAN configuration-2](/img/wan_2.png)

* Configure LAN interface (you can use any other private ip range, I am using 192.168.1.0/24)

![LAN configuration-1](/img/lan_1.png)

![LAN configuration-2](/img/lan_2.png)

## Setup DHCP Server
```
sudo apt-get install isc-dhcp-server
```
**configure dhcp server with the interface it should be listening to**
```
sudo nano /etc/default/dhcpd.conf
```
set the following value
> INTERFACES="*enp2s0*"

**configure dhcp server**
It is better to backup the configuration file before editing. Start editin the file using the command
```
sudo nano /etc/dhcp/dhcpd.conf
```
Replace whatever in the file with the following data

> option domain-name "*whatever.you.want*"; <br/> 
> option domain-name-servers *8.8.8.8, 8.8.4.4*; <br/> 
> default-lease-time 600; <br/>
> max-lease-time 7200; <br/>
> ddns-update-style none; <br/>
> authoritative; <br/>
> log-facility local7; <br/>
> subnet *192.168.1.0* netmask *255.255.255.0* { <br/>
>&nbsp;&nbsp;&nbsp;&nbsp; range *192.168.1.101 192.168.1.200*; <br/>
>&nbsp;&nbsp;&nbsp;&nbsp; option subnet-mask *255.255.255.0*; <br/>
>&nbsp;&nbsp;&nbsp;&nbsp; option routers *192.168.1.1*; <br/>
>&nbsp;&nbsp;&nbsp;&nbsp; option broadcast-address *192.168.1.255*; <br/>
> }

* please adjust *italicized* text according to your setup

**restart and enable isc-dhcp-server service**
```
sudo systemctl restart isc-dhcp-server
sudo systemctl enable isc-dhcp-server
```
check service status using the command:
```
sudo systemctl status isc-dhcp-server
```

## Configure Firewall 
Add the following iptables rules to /etc/rc.local (create one, if it doesn't exist)
```
sudo nano /etc/rc.local
```
> &#35;!/bin/bash
> 
> &#35; /etc/rc.local
> 
> &#35; Default policy to drop all incoming packets.
> iptables -P INPUT DROP
> iptables -P FORWARD DROP
> 
> &#35; Accept incoming packets from localhost and the LAN interface.
> iptables -A INPUT -i lo -j ACCEPT
> iptables -A INPUT -i *enp2s0* -j ACCEPT
> 
> &#35; Accept incoming packets from the WAN if the router initiated the connection.
> iptables -A INPUT -i *enp1s0* -m conntrack \
> --ctstate ESTABLISHED,RELATED -j ACCEPT
> 
> &#35; Forward LAN packets to the WAN.
> iptables -A FORWARD -i *enp2s0* -o *enp1s0* -j ACCEPT
> 
> &#35; Forward WAN packets to the LAN if the LAN initiated the connection.
> iptables -A FORWARD -i *enp1s0* -o *enp2s0* -m conntrack \
> --ctstate ESTABLISHED,RELATED -j ACCEPT
> 
> &#35; NAT traffic going out the WAN interface.
> iptables -t nat -A POSTROUTING -o *enp1s0* -j MASQUERADE
> 
> &#35; rc.local needs to exit with 0
> exit 0


* please adjust *italicized* text according to your setup
set rc.local permissions 

```
sudo chmod 755 /etc/rc.local
```

## Enable packet forwarding (for IPv4)
```
sudo nano /etc/rc.local
```
 Uncomment the line:
>  net.ipv4.ipforward=1

## Reboot

That's it. 

*4 / 21 days of continous posts on this website*

```
𝗧𝗼𝗱𝗮𝘆' 𝗖𝗵𝗲𝗰𝗸𝗹𝗶𝘀𝘁
🗹 Finger Whistling
🗹 Blog
▣ Bending
▣ Flute
🗹  Electronics
☐ Meditation
☐ Vim
🗹 Chess
```
