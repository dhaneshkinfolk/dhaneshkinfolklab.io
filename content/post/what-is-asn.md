---
title: "Autonomous System Number (ASN)"
date: 2019-01-28T22:31:19+05:30
categories:
- networking
tags:
- l2s2l
- todo
keywords:
- tech
#thumbnailImage: //example.com/image.jpg
---
I came to know about ASN two days ago, when my friend asked me for help with one of his projects. Eventhough I was forced to do a class on CCNA during my college days, I had no idea about this term. As usual, I went in search of answers.
<!--more-->
ASNs are one of the building blocks of the Internet. In simple terms, it is the identification number for the networks in the *network of networks*(that is how everyone defines the Internet). Let us start by defining the AS in ASN.
### Autonomous System
AS or Autonomous System is a Network (or collection of Networks) that is managed by single entity (or multiple providers on  behalf of a single entity) and has a well defined policy to the Internet. The core Internet routes packets to the AS and it is the responsibility of the AS to route it to the correct node.

### Autonomous System Number(ASN)
ASNs are globally unique numbers assigned to Autonomous Systems. ASNs are assigned in blocks [Internet Assigned Numbers Authority (IANA)](https://en.wikipedia.org/wiki/Internet_Assigned_Numbers_Authority) to Regional Internet Registries(RIR). RIR is the organization responsible for allocation and registration of IP addresses and ASNs in a region of the world. Currently there are 5 RIRs (visit https://en.wikipedia.org/wiki/Regional_Internet_registry for more details). RIR assings ASN  on request([Benefits of having on ASN](https://www.mumbai-ix.net/blog/why-network-should-use-own-autonomous-system-number-asn/)). Unti 2007, 16bit ASNs where used. 32 bit ASNs were introduced in 2007 to meet the increasing number of networks.

ASNs perform core routing by exchanging routing table with one another. ANSs exchange information among themselves using BGP(Broader Gateway Protocol). There are two types of ASNs -
**Public ASN:** A public ASN is used when the system is exchanging routing information on the public Internet with other AS. **Private ASN:** It is used when the AS only communicating with a single provider via BGP. If you are really interested in understanding, how the BGP routers interconnect Autonomous Systems (AS), search for a BGP Looking Glass. You can get your live ASN details by visiting https://ipinfo.io .

I did not fully understand all the conceps. I may have to create a small model of internet or dive into the backends of Internet(I don't even know what it means) to get a better idea. As it is said - *I hear and I forget. I see and I remember. I do and I understand.*

*2 / 21 days of continous posts on this website*
