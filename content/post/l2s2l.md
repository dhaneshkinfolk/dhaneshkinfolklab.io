---
title: "L2s2l"
date: 2019-01-28T00:24:43+05:30
categories:
- category
- subcategory
tags:
- tag1
- tag2
keywords:
- tech
draft: true
#thumbnailImage: //example.com/image.jpg
---

<!--more-->

न चोरहार्यं न च राजहार्यं 
न भ्रातृभाज्यं न च भारकारि ।
व्यये कृते वर्धत एव नित्यं 
विद्याधनं सर्वधनप्रधानम् ॥

 If you have an apple and I have an apple and we exchange apples then you and I will still each have one apple. But if you have an idea and I have an idea and we exchange these ideas, then each of us will have two ideas.

George Bernard Shaw 
