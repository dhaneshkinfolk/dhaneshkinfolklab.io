---
title: "Publish a Hugo Website with GitLab Pages"
date: 2019-01-25T23:05:06+05:30
categories:
- article
#- subcategory
tags:
- l2s2l
- hugo
keywords:
- tech
#thumbnailImage: //example.com/image.jpg
---
I have been trying to publish this site for past one day. I have used github pages before and it was as simple as it can get. I finally managed to do it after finding the right steps after fiddling with it for one day. If you are in a hurry go [here](#steps)
<!--more-->
I started the day on a good note. Woke up early and had breakfast from a place, I have been planning to eat for a long time. Then I found the simplest instructions to setup hugo website with gitlab pages, which just worked (unlike the original article I followed). 

First things first, gitlab user account (there is also group account) can have two types of websites (I am not sure if the word website is correct usage - anyways it converys idea, that is more than enough for me.=) - user website and project website. User website will have the address `<namespace>.gitlab.io` (where namespace is just your username or group name, if you created this project under a group) and project website will have the address `<namespace>.gitlab.io/<project-name>`. Most articles online mentions this at the end of the article - I am not sure about the official documentation, I usually avoid it, official documentations are usually too long with lots of texts and lots of hyperlinks(It is too much for me!! :( ). I was trying ot publish a [user website](https://dhaneshkinfolk.gitlab.io) (Is it a user or an user <assume a thinking smiley here, I don't know how to make one :p>), but following the articles online left me with a project website. Eventhough I found a video on gitlab's official youtube page explaning the conversion, differnces in thereference inteface used in the video and current gitlab interface made things difficult(and I was half asleep as well). Some settings felt ambiguous and difficult to find. Below are the steps I followed to get the site running(It is taken from [hugo's official docs](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/) for the same).

## Steps to Publish a Hugo Website with GitLab Pages<a name="steps"></a>
* create a new repository. 
  * Name it `<your-username>.gitlab.io`, **if you are trying to setup a user website**
  * visit `https://gitlab.com/<your-username>/<your-project>/pages` to confirm the address on which the page is served.
* [create a hugo website](https://gohugo.io/getting-started/quick-start/) on your computer
* create `.gitlab-ci.yml` in your  hugo website directory with following content
  ```
  image: monachus/hugo
  
  variables:
      GIT_SUBMODULE_STRATEGY: recursive
  
  pages:
    script:
    - hugo
    artifacts:
      paths:
      - public
    only:
    - master
  ```

* initialize new git repository in hugo website directory
```
  git init
```
* add /public directory to our .gitignore file
```
  echo "/public" >> .gitignore
```
* commit and push code to master branch
```
  git add .
  git commit -m "Initial commit"
  git remote add origin https://gitlab.com/<your-username>/<your-hugo-site>.git
  git push -u origin master
```

**That's it**

Wait till your page gets built (for live status visit `https://gitlab.com/<your-username>/<your-hugo-site>/pipelines`). 

### For the curious ones
* The `.gitlab-ci.yml` file is part of the GitLab's built-in Continuous Integration system. You can read more about it [here](https://docs.gitlab.com/ee/ci/README.html)
* Details on the contents of the file `.gitlab-ci.yml` can be found [here](https://docs.gitlab.com/ee/user/project/pages/introduction.html#how-gitlab-ciyml-looks-like-when-using-a-static-generator)


*OK.. It is day 2 of 21 (I am trying to write on this website continously for atleast 21 days).*
