---
title: "About"
date: 2019-01-24T01:16:18+05:30
# categories:
# - category
# - subcategory
# tags:
# - tag1
# - tag2
keywords:
- intro
#thumbnailImage: //example.com/image.jpg
---

<!--more-->
## Waiting for the right moment
All my life I have been waiting for the right moment or the right thing to happen but it never arrived or happened. A few days ago I was looking back at my life and I realised that it was full of things I wished to do but never did. I was busy planning these things and my life went by. As John Lennon rightly said *Life is what happens while you're busy planning other things*. Some of these plans are over 20 years old and I am only 27. Some of them may seem trivial to others, but I wished to do it and never did it, so it is important for me. 

Either I was too lazy to try or too afraid to fail, whatever be the reason I kept on procastinating. So I decided to change that and decided to build this website to keep track of the progress. I am not sure about the categorization of this site. It can be called a blog or a diary or whatever you like. Some of the posts could be just lame excuses to myself. Currently I am setting up this website using gitlab and hugo. I used an [article](https://netdevops.me/2017/setting-up-a-hugo-blog-with-gitlab-and-cloudflare/#) from Roman Dodin as reference. I came across the article 4 days ago and decided to build this website and have been putting it off till now. I haven't even read the article until a few minutes ago.

This is not the first time I am getting enlightened(:P). Most of the enlightenment will be over after one sleep. Sometimes it lasts 2-3 days, never more than that. I have even tried [blogging](https://dhaneshkinfolk.blogspot.com/) and diary writing before but nothing sticked. This time I wish to continue it at least for 21 days. It is said to take 21 days to make a new habit(It could be a myth, but let me start with something... In the future I may research it and write an article about it.)

## Why am I writing in English
English is neither my first language nor my strong point. People who knows me personally knows that I suck at it. Still I chose English because it is either english or malayalam (I am not a fan of manglish - using english script for malayalam) and I don't know malayalam typing. Next option is transliteration but it is time consuming and frustating. 
